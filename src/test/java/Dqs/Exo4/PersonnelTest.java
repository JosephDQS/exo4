package Dqs.Exo4;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.Month;

import org.junit.Test;

public class PersonnelTest {
	/**
	 * TestInstancePersonnel() qui test la creation de Personnel
	 */
	@Test
	public void TestInstancePersonnel() {
		Personnel P = new Personnel
				.Builder("Desquaires", "Joseph")
				.setDoB(LocalDate.of(1997, Month.NOVEMBER, 23))
				.addnum("06XXYYZZ09")
				.build();
		assertNotNull(P);
	}
	
	@Test
	public void TestNomPersonnel() {
		Personnel P = new Personnel
				.Builder("Desquaires", "Joseph")
				.setDoB(LocalDate.of(1997, Month.NOVEMBER, 23))
				.addnum("06XXYYZZ09")
				.build();
		assertEquals(P.getNom(),"Desquaires");
	}

	@Test
	public void TestPrenomPersonnel() {
		Personnel P = new Personnel
				.Builder("Desquaires", "Joseph")
				.setDoB(LocalDate.of(1997, Month.NOVEMBER, 23))
				.addnum("06XXYYZZ09")
				.build();
		assertEquals(P.getPrenom(),"Joseph");
	}
}
