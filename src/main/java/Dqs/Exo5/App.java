package Dqs.Exo5;

import java.time.LocalDate;
import java.time.Month;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
       Personnel P1 = new Personnel
    		   	.Builder("Desquaires", "Joseph")
				.setDoB(LocalDate.of(1997, Month.NOVEMBER, 23))
				.addnum("06XXYYZZ09")
				.build();
       
       Personnel P2 = new Personnel
   		   		.Builder("Robert", "Romain")				
				.build();
       
       Personnel P3 = new Personnel
  		   		.Builder("Lledos", "Nicolas")
  		   		.setDoB(LocalDate.of(1996, Month.JANUARY, 16))
				.build();
       
       Personnel P4 = new Personnel
   		   	.Builder("Piguet", "Adrien")
				.addnum("07XXYYZZ39")
				.build();
       
       CompositePersonnel CP1 = new CompositePersonnel();
       
       CP1.add(P1);
       CP1.add(P2);
       CP1.add(P3);
       CP1.add(P4);
       CP1.affiche();
       
    }
}
