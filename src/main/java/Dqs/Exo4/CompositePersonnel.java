package Dqs.Exo4;

import java.util.ArrayList;

public class CompositePersonnel implements Humain{
	private ArrayList<Humain> childHumain = new ArrayList<Humain>() ;
	
	@Override
	public void affiche() {
		for (Humain humain : childHumain) {
			humain.affiche();
		}
	}
	
	public ArrayList<Humain> getList()
	{
		return this.childHumain;
	}
	
	public void add(Humain H)
	{
		childHumain.add(H);
	}
	
	public void remove(Humain H)
	{
		childHumain.remove(H);
	}
}
