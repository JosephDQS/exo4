package Dqs.Exo4;

import java.sql.PreparedStatement;
import java.sql.*;

public class PersonnelDAO extends DAO<Personnel>{
@Override public Personnel create(Personnel obj) {
	try {
		PreparedStatement prepare = connect.prepareStatement(
				"INSERT INTO personnels(nom,prenom,DoB) VALUES(? , ?, ?)");
		prepare.setString(1, obj.getNom());
		prepare.setString(2, obj.getPrenom());
		prepare.setString(3, obj.getDoB());
		int result = prepare.executeUpdate();
		assert result == 1;
	}
	catch(SQLException e) {
		e.printStackTrace();
	}
	return obj;
}
@Override public Personnel find(String id) {
	Personnel p = new Personnel(null);
	try {
		PreparedStatement prepare = connect.prepareStatement(
				"SELECT * FROM personnels WHERE nom = ?");
		prepare.setString(1, id);
		ResultSet result = prepare.executeQuery();
		if(result.first()) {
			p = new personnel(
					result.getString("nom"),
					result.getString("prenom"));
		}
	} catch(SQLException e ) {
		e.printStackTrace();
	}
	return p;
}
@Override public Personnel update(Personnel obj) {/* ....*/;}
@Override public void delete(Personnel obj) {/* ....*/;}
}