package Dqs.Exo4;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;

public class Personnel implements Humain{
	private final String Nom;
	private final String Prenom; 
	private final LocalDate DoB;
	private final ArrayList<String> Numeros;
	
	public static class Builder {
			//required parameters
				private final String Nom;
				private final String Prenom; 
						
			//Optional parameters
				private LocalDate DoB = LocalDate.of(0000,Month.JANUARY,01);
				private ArrayList<String> Numeros = new ArrayList<String>();
				
				public Builder(String Nom, String Prenom) {
					this.Nom = Nom;
					this.Prenom = Prenom;				
				}
				public Builder setDoB(LocalDate DoB) {
					this.DoB = DoB;
					return this;
				}
				public Builder addnum(String num){
					this.Numeros.add(num);
					return this;
				}
				
				public Personnel build() {
					return new Personnel(this);
				}
	}
	
	private Personnel(Builder builder)
	{
		//required parameters
		this.Nom = builder.Nom;
		this.Prenom = builder.Prenom;
		this.DoB = builder.DoB;
		this.Numeros = new ArrayList<String>(builder.Numeros);
	}

	public String getPrenom() {
		return Prenom;
	}

	public String getNom() {
		return Nom;
	}

	public String getDoB() {
		return DoB.toString();
	}

	public ArrayList<String> getNumeros() {
		return Numeros;
	}
	@Override
	public void affiche()
	{
		System.out.print(Nom +" "+Prenom + "\n");
	}
}

